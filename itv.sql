Using default implementation for ThreadExecutor
Job execution threads will use class loader of thread: main
Initialized Scheduler Signaller of type: class org.quartz.core.SchedulerSignalerImpl
Quartz Scheduler v.2.2.1 created.
RAMJobStore initialized.
Scheduler meta-data: Quartz Scheduler (v2.2.1) 'DefaultQuartzScheduler' with instanceId 'NON_CLUSTERED'
  Scheduler class: 'org.quartz.core.QuartzScheduler' - running locally.
  NOT STARTED.
  Currently in standby mode.
  Number of jobs executed: 0
  Using thread pool 'org.quartz.simpl.SimpleThreadPool' - with 10 threads.
  Using job-store 'org.quartz.simpl.RAMJobStore' - which does not support persistence. and is not clustered.

Quartz scheduler 'DefaultQuartzScheduler' initialized from default resource file in Quartz package: 'quartz.properties'
Quartz scheduler version: 2.2.1
Scheduler DefaultQuartzScheduler_$_NON_CLUSTERED started.
An error occured instantiating job to be executed. job= 'JOB_GROUP.test'
org.quartz.SchedulerException: Problem instantiating class 'com.xj.spring.quartz.TestJob' [See nested exception: java.lang.InstantiationException: com.xj.spring.quartz.TestJob]
	at org.quartz.simpl.SimpleJobFactory.newJob(SimpleJobFactory.java:58)
	at org.quartz.simpl.PropertySettingJobFactory.newJob(PropertySettingJobFactory.java:69)
	at org.quartz.core.JobRunShell.initialize(JobRunShell.java:127)
	at org.quartz.core.QuartzSchedulerThread.run(QuartzSchedulerThread.java:375)
Caused by: java.lang.InstantiationException: com.xj.spring.quartz.TestJob
	at java.lang.Class.newInstance(Class.java:359)
	at org.quartz.simpl.SimpleJobFactory.newJob(SimpleJobFactory.java:56)
	... 3 more
