#!/bin/bash
#author:xiajun
#date:2014-02-27
path=$(cd `dirname $0`;pwd)
libPath="$path/../lib"
echo "LIBPATH: $libPath"
confPath="$path/../conf"
echo "CONFPATH: $confPath"
logsPath="$path/../logs"
echo "LOGSPATH: $logsPath"
CLASSPATH=""
PID="$path/PID"
conf="$path/../conf/conf.properties"
MAINCLASS=`sed '/MAINCLASS/!d;s/.*=//' $conf | tr -d '\r'`
echo "MAINCLASS: $MAINCLASS"
pid=`ps  --no-heading -C java -f --width 1000 | grep $MAINCLASS |awk '{print $2}'`
if [[ $pid -gt 0 ]]; then
    echo "************************************************"
    echo "*                                              *"
    echo "*                                              *"
    echo "*  ERROR: The service has been start,pid=$pid. *"
    echo "*                                              *"
    echo "*                                              *"
    echo "************************************************"
    exit 1
fi

if [ ! -d $logsPath ]; then
    mkdir -p $logsPath
fi
if [ -d $libPath ]; then
    for jar in $libPath/*.jar
    do
        CLASSPATH=$jar:$CLASSPATH
    done
fi
if [ -d $confPaht ]; then
    CLASSPATH=$CLASSPATH$confPath
fi
#CLASSPATH=$BASE_DIR/conf:$(ls $BASE_DIR/lib/*.jar | tr '\n' :)
echo "CLASSPATH: $CLASSPATH"
JAVA_OPTS=" -Djava.awt.headless=true -Djava.net.preferIPv4Stack=true "
JAVA_DEBUG_OPTS=""
if [ "$1" = "debug" ]; then
    JAVA_DEBUG_OPTS=" -Xdebug -Xnoagent -Djava.compiler=NONE -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n "
fi
JAVA_JMX_OPTS=""
if [ "$1" = "jmx" ]; then
    JAVA_JMX_OPTS=" -Dcom.sun.management.jmxremote.port=1099 -Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.authenticate=false "
fi
JAVA_MEM_OPTS=""
BITS=`file $JAVA_HOME/bin/java | grep 64-bit`
if [ -n "$BITS" ]; then
    let memTotal=`cat /proc/meminfo |grep MemTotal|awk '{printf "%d", $2/1024 }'`
    if [ $memTotal -gt 2500 ]; then
        JAVA_MEM_OPTS=" -server -Xmx4096m -Xms4096m -Xmn512m -XX:PermSize=256m -Xss2048k -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:LargePageSizeInBytes=128m -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70 "
    else
        JAVA_MEM_OPTS=" -server -Xmx1024m -Xms1024m -Xmn256m -XX:PermSize=128m -Xss256k -XX:+DisableExplicitGC -XX:+UseConcMarkSweepGC -XX:+CMSParallelRemarkEnabled -XX:+UseCMSCompactAtFullCollection -XX:LargePageSizeInBytes=128m -XX:+UseFastAccessorMethods -XX:+UseCMSInitiatingOccupancyOnly -XX:CMSInitiatingOccupancyFraction=70 "
    fi
else
    JAVA_MEM_OPTS=" -server -Xms1024m -Xmx1024m -XX:PermSize=128m -XX:SurvivorRatio=2 -XX:+UseParallelGC "
fi
nohup java $JAVA_OPTS $JAVA_MEM_OPTS $JAVA_DEBUG_OPTS $JAVA_JMX_OPTS -cp $CLASSPATH $MAINCLASS > $logsPath/stdout.log 2>&1 &
code=$?
if [ code==0 ];then
    echo "start success!"
else
    echo "start error!"
fi
echo $! > $PID
echo "PID = `cat $PID`"
echo "LOGS = $logsPath/stdout.log"
exit "$code"