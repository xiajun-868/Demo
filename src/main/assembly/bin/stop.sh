#!/bin/bash
#author:xiajun
#date:2014-02-27
path=$(cd `dirname $0`;pwd)
conf="$path/../conf/conf.properties"
MAINCLASS=`sed '/MAINCLASS/!d;s/.*=//' $conf | tr -d '\r'`
pid=`ps  --no-heading -C java -f --width 1000 | grep $MAINCLASS |awk '{print $2}'`
if [ -z "$pid" ]; then
    echo "ERROR: The $MAINCLASS service does not started"
    exit 1
fi
kill $pid
COUNT=0
while [ $COUNT -lt 1 ]; do
    echo -e ".\c"
    sleep 1
    COUNT=1
    for PID in $pid ; do
        PID_EXIST=`ps --no-heading -p $PID`
        if [ -n "$PID_EXIST" ]; then
            COUNT=0
            break
        fi
    done
done
echo "STOP OK!"
