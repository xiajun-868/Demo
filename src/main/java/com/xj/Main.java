package com.xj;

import com.xj.spring.quartz.QuartzDemo;
import com.xj.spring.quartz.TestJob;
import org.quartz.SchedulerException;
import org.slf4j.spi.LocationAwareLogger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

/**
 * User: bjxiajun
 * Date: 14-5-4
 * Time: 下午1:58
 */
public class Main {
    private String userName;

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }
   // LocationAwareLogger
    public static void main(String[] args) throws SchedulerException {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:spring/applicationContext.xml");
        //Main service = (Main) context.getBean("demo");
        //System.out.println(service.getUserName()+"---------------------");
        QuartzDemo d=(QuartzDemo)context.getBean("quartzDemo");
        d.addJob("test",new Date(new Date().getTime()+3000),new TestJob(),"fffffffffffff");
    }
}
