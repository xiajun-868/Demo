package com.xj.cache;

import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * User: xia
 * Date: 14-3-17
 * Time: 下午8:39
 * 延迟阻塞对象
 */
public class CacheDelay implements Delayed {
    private Item item;
    private long second;
    private long createTime;//创建时间

    /**
     * 缓存对象,缓存时间由传入参数second控制
     *
     * @param item   缓存对象
     * @param second 缓存多长时间
     */
    public CacheDelay(Item item, long second) {
        this.item = item;
        this.second = TimeUnit.NANOSECONDS.convert(second, TimeUnit.SECONDS);
        this.createTime = System.nanoTime() + this.second;
    }

    /**
     * 缓存对象以String类型对象为key，此key缓存时间为60分钟
     */
    public CacheDelay(Item item) {
        this(item, 60 * 60);
    }

    @Override
    public long getDelay(TimeUnit unit) {
        return unit.convert(createTime - System.nanoTime(), TimeUnit.NANOSECONDS);
    }

    @Override
    public int compareTo(Delayed item) {
        if (this == item) {
            return 0;
        }
        if (item instanceof CacheDelay) {
            CacheDelay cd = (CacheDelay) item;
            long diff = createTime - cd.createTime;
            if (diff < 0) {
                return -1;
            } else if (diff > 0) {
                return 1;
            } else {
                return 0;
            }
        }
        long d = (getDelay(TimeUnit.NANOSECONDS) - item.getDelay(TimeUnit.NANOSECONDS));
        return (d == 0) ? 0 : ((d < 0) ? -1 : 1);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof CacheDelay){
            CacheDelay cd = (CacheDelay) obj;
            long diff = createTime - cd.createTime;
            return diff==0;
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return Long.valueOf(createTime).hashCode();
    }

    /**
     * 返回缓存id
     *
     * @return
     */
    public Item getItem() {
        return item;
    }
}
