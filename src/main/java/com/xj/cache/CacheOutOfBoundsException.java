package com.xj.cache;

/**
 * User: xia
 * Date: 14-3-18
 * Time: 下午10:14
 * 缓存大小超出限制异常
 */
public class CacheOutOfBoundsException extends Exception {
    public CacheOutOfBoundsException(){
        super();
    }
    public CacheOutOfBoundsException(String message){
        super(message);
    }
}
