package com.xj.cache;

/**
 * User: xia
 * Date: 14-3-18
 * Time: 下午9:55
 */
public class CacheTimeOutCheckException extends Exception {
    public CacheTimeOutCheckException(){
        super();
    }
    public CacheTimeOutCheckException(String message){
        super(message);
    }
}
