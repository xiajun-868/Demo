package com.xj.cache;

/**
 * User: xia
 * Date: 14-3-17
 * Time: 下午10:00
 */
public class Item {
    private String key;
    private Object obj;
    public Item(){}
    public Item(String key,Object obj){
        this.key=key;
        this.obj=obj;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }
}
