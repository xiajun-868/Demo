package com.xj.cache;

import org.apache.log4j.Logger;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: xia
 * Date: 14-3-17
 * Time: 下午8:37
 */
public class MyCache {
    private final static Logger LOG = Logger.getLogger(MyCache.class);
    private AtomicInteger atomic = new AtomicInteger(0);
    private ConcurrentMap<String, Object> cacheMap = new ConcurrentHashMap<String, Object>(1024);
    private DelayQueue<CacheDelay> queue = new DelayQueue<CacheDelay>();
    private int size;

    /**
     * 构造函数
     *
     * @param size 缓存的大小 -1为不设上限
     */
    public MyCache(int size) {
        this.size = size;
        Thread cacheThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    check();
                } catch (CacheTimeOutCheckException e) {
                    e.printStackTrace();
                }
            }
        });
        cacheThread.setDaemon(true);
        cacheThread.setName("MyCacheDaemon");
        cacheThread.start();
    }

    public MyCache() {
        this(-1);
    }

    /**
     * 添加缓存
     *
     * @param key    缓存标识
     * @param obj    缓存的对象
     * @param second 缓存的时间秒为单位
     */
    public void set(String key, Object obj, long second) throws CacheOutOfBoundsException {
        if (size != -1 && atomic.incrementAndGet() > size) {
            throw new CacheOutOfBoundsException("缓存最大size=" + size + ",当前缓存对象为" + atomic.get());
        }
        CacheDelay cacheDelay = new CacheDelay(new Item(key, obj), second);
        Object o = cacheMap.put(key, cacheDelay);
        if (o != null) {
            boolean b = queue.remove(o);
            LOG.debug("key: 【" + key + "】 缓存的对象被更新,操作返回 " + b);
        }
        queue.put(cacheDelay);
    }

    /**
     * 获取缓存对象
     *
     * @param key 缓存标识
     * @return Object 缓存的对象
     */
    public Object get(String key) {
        Object obj = cacheMap.get(key);
        if (obj != null) {
            return ((CacheDelay) obj).getItem().getObj();
        }
        return null;
    }


    /**
     * 检查缓存对象是否到期
     *
     * @throws CacheTimeOutCheckException 检查时出现的阻塞队列异常
     */
    private void check() throws CacheTimeOutCheckException {
        for (; ; ) {
            try {
                CacheDelay cacheDelay = queue.take();
                if (cacheDelay != null) {
                    Item item = cacheDelay.getItem();
                    cacheMap.remove(item.getKey());
                    if (size != -1) {
                        atomic.decrementAndGet();
                    }
                }
            } catch (InterruptedException e) {
                LOG.error("检查【缓存超时线程】运行时出现异常.", e);
                throw new CacheTimeOutCheckException("检查缓存超时线程运行时出现异常");
            }
        }
    }

    /**
     * 清空缓存系统，以前所有缓存都将被清除
     */
    public void clear() {
        cacheMap.clear();
        queue.clear();
    }

    public static void main(String[] args) throws InterruptedException, CacheOutOfBoundsException {
        MyCache cache = new MyCache(12);
        cache.set("1", "123", 2);
        cache.set("1", "234", 2);
        System.out.println(cache.get("1"));
        Thread.sleep(3000);
        System.out.println(cache.get("1"));
    }
}
