/*
 * Copyright (c) 2016. mogujie
 */

package com.xj.datastructure;

/**
 * Author: baichuan - xiajun
 * Date: 16/09/19 11:14
 * 打印二维数组对角线
 *  1 2 3
 *  4 5 6
 *  7 8 9
 *  打印：
 *  1
 *  24
 *  357
 *  68
 *  9
 *  分析：下标对应
 *  00 01 02
 *  10 11 12
 *  20 21 22
 * i+j=c
 * c相同的就是需要一起打印出来的
 *
 */
public class PrintArrayLine {
    public static void printArray() {
        int n = 10; //10 x 10 的二维数组
        int[][] arr = new int[n][n];
        int k = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                arr[i][j] = k++;
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("---------------------------------------------------------------");
        StringBuilder sb = new StringBuilder();
        long stime = System.currentTimeMillis();
        int c = 2 * n - 1;//打印总次数，也是i+j的值
        for (int f = 0; f < c; f++) {
            int min = n > f ? f : n - 1;
            int start = f >= n ? (f - n + 1) : 0;
            for (int i = start; i <= min; i++) {//每一个数有多少中组合方式 如：3＝(0+3 3+0 1+2 2+1)
                int j = f - i;
                sb.append(arr[i][j]).append(" ");
            }
            sb.append("\r\n");
        }
        String use = (System.currentTimeMillis() - stime) + "ms";
        System.out.println(sb.toString());
        System.out.println(use);
    }
}
