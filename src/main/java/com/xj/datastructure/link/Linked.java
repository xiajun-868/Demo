/*
 * Copyright (c) 2016. mogujie
 */

package com.xj.datastructure.link;

/**
 * Author: baichuan - xiajun
 * Date: 16/09/19 11:09
 * 反转链表
 */
public class Linked {
    public static void reverseNode() {
        Node root = new Node("1");
        Node n2 = new Node("2");
        Node n3 = new Node("3");
        Node n4 = new Node("4");
        Node n5 = new Node("5");
        root.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;
        n5.next = root;

        Node tmp = root.next;
        Node f = root;
        f.next = null;
        while (tmp != null) {
            Node n = tmp.next;
            tmp.next = f;
            if (n != null) {
                f = tmp;
            }
            tmp = n;
        }
        while (f != null) {
            System.out.println(f.value);
            f = f.next;
        }
    }

    static class Node {
        public Node(String value) {
            this.value = value;
        }

        public Node next;
        public String value;
    }
}
