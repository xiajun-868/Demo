package com.xj.datastructure.link;

/**
 * Author: xiajun
 * Date: 2015-04-15
 * Time: 19:38:00
 */
public class Node {
    public Node(int v) {
        this.v = v;
    }

    public int v = 0;
    public Node next;

    public static void main(String[] args) {
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        head.next.next.next.next = new Node(5);
        /*Node n1 = new Node(head.v);
        while (head != null) {
            Node tmp = n1.next;
            n1.next=head;
            head=head.next;
            n1.next.next=tmp;
        }
        n1=n1.next;*/
        Node n1 = null;
        Node n2 = null;
        while(head!=null){
            n1=head.next;
            head.next=n2;
            n2=head;
            head=n1;
        }
        head=n2;
        while (head != null) {
            System.out.println(head.v);
            head = head.next;
        }
    }
}
