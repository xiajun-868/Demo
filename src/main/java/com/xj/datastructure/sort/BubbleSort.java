package com.xj.datastructure.sort;

/**
 * author: xiajun
 * Date: 2014-07-19
 * Time: 13-28-00
 * 冒泡排序 优化
 */
public class BubbleSort {
    public static void main(String[] args) {
        int a[]={2,4,42,43,145,211};
        BubbleSort sort=new BubbleSort();
        sort.sort(a);
        for (int i = 0; i < a.length; i++) {
            System.out.println(a[i]);
        }
    }
    public void sort(int[] a) {
        boolean isok = false;
        int count=0;
        for (int i = 0; i < a.length; i++) {
            isok=false;
            for (int j = 0; j < a.length-i-1; j++) {
                count++;
                if(a[j]>a[j+1]){
                    int tmp=a[j];
                    a[j]=a[j+1];
                    a[j+1]=tmp;
                    isok=true;
                }
            }
            if(!isok){
                break;
            }
        }
        System.out.println("count: "+count);
    }

}
