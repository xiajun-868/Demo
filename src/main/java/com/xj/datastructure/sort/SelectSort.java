package com.xj.datastructure.sort;

/**
 * 选择排序
 * 首先在未排序序列中找到最小（大）元素，存放到排序序列的起始位置，然后，
 * 再从剩余未排序元素中继续寻找最小（大）元素，然后放到已排序序列的末尾。以此类推，直到所有元素均排序完毕。
 */
public class SelectSort
{
	public static void main(String[] args){
		int arr[]={3,5,72,1,7,43,9,2};
		for(int i=0;i<arr.length;i++){
			int j=sort(arr,i);
			if(i!=j){
				int tmp=arr[i];
				arr[i]=arr[j];
				arr[j]=tmp;
			}
		}
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
		
	}
	public static int sort(int[]arr,int index){
		
		int j=index;
		int tmp=arr[j];
		for(int i=index+1;i<arr.length;i++ ){
			if(arr[i]<tmp){
				tmp=arr[i];
				j=i;
			}
		}
		return j;
	}
}