package com.xj.hadoop.hive;

import java.sql.*;

/**
 * hive jdbc 连接测试类
 * @author xiajun
 *
 */
public class HiveDemo {
	private Connection conn=null;
	private PreparedStatement ps=null;
	private static String driverName = "org.apache.hadoop.hive.jdbc.HiveDriver";
	
	private void connection() throws ClassNotFoundException, SQLException{
		Class.forName(driverName);
		conn = DriverManager.getConnection("jdbc:hive://10.12.147.196:10000/default", "", "");
	}
	public PreparedStatement createStatement(String sql) throws Exception{
		this.connection();
		ps=conn.prepareStatement(sql);
		//conn.close();
		return ps;
	}
	public static void main(String[] args) throws Exception {
		HiveDemo hive=new HiveDemo();
		PreparedStatement ps=hive.createStatement("select * from h_test2");
		ResultSet res =ps.executeQuery();
		while(res.next()){
			int id=res.getInt(1);
			String name=res.getString(2);
			System.out.println("====="+id+"===="+name+"  "+res.getString(3));
		}
        res.close();
	}
}
