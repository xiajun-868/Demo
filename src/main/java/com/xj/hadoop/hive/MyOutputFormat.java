package com.xj.hadoop.hive;

import org.apache.hadoop.hive.ql.exec.ByteWritable;
import org.apache.hadoop.hive.ql.exec.FileSinkOperator.RecordWriter;
import org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Writable;

import java.io.IOException;


public class MyOutputFormat extends HiveIgnoreKeyTextOutputFormat<LongWritable, ByteWritable>{
   static class MyRecordWriter implements RecordWriter{

	@Override
	public void write(Writable w) throws IOException {
		
	}

	@Override
	public void close(boolean abort) throws IOException {
		
	}
	   
   }
}
