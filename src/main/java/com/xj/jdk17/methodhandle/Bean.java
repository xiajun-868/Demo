package com.xj.jdk17.methodhandle;

/**
 * User: bjxiajun
 * Date: 14-3-6
 * Time: 下午2:13
 */
public class Bean {
    private String name;
    private int age;
    private Float money;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Float getMoney() {
        return money;
    }

    public void setMoney(Float money) {
        this.money = money;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
