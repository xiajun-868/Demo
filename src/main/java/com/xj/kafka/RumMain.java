package com.xj.kafka;

import com.xj.kafka.consumer.ItemBrowsingConsumer;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * User: bjxiajun
 * Date: 13-10-18
 * Time: 下午1:42
 */
public class RumMain {
    private final static Logger log = Logger.getLogger(RumMain.class);

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/spring-kafka-consumer.xml");
        ItemBrowsingConsumer consumer = (ItemBrowsingConsumer) context.getBean("itemBrowsingConsumer");
        log.info(">>>>>>>>>>>>>>>>>>>>>>>START<<<<<<<<<<<<<<<<<<<<<<<");
        consumer.run();
    }
}
