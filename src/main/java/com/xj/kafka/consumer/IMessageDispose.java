package com.xj.kafka.consumer;

/**
 * 处理接收到的消息
 * User: bjxiajun
 * Date: 13-10-18
 * Time: 下午3:09
 */
public interface IMessageDispose {
    /**
     * 处理消息的方法
     * @param obj
     */
    public void dispose(Object obj);
}
