package com.xj.kafka.thread;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * User: bjxiajun
 * Date: 13-10-18
 * Time: 下午1:10
 */
public class MessageDisposePoolFactory implements ThreadFactory {
    static final AtomicInteger poolNumber = new AtomicInteger(1);

    @Override
    public Thread newThread(Runnable r) {
        String name = "jdMessagePool-" + poolNumber.getAndIncrement();
        Thread thread = new Thread(r, name);
        thread.setDaemon(true);
        return thread;
    }
}
