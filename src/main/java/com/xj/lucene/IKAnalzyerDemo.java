package com.xj.lucene; /**
 * IK 中文分词  版本 5.0.1
 * IK Analyzer release 5.0.1
 *
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * 源代码由林良益(linliangyi2005@gmail.com)提供
 * 版权声明 2012，乌龙茶工作室
 * provided by Linliangyi and copyright 2012 by Oolong studio
 *
 *
 */

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.TypeAttribute;
import org.wltea.analyzer.cfg.DefaultConfig;
import org.wltea.analyzer.lucene.IKAnalyzer;
import java.io.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * 使用IKAnalyzer进行分词的演示
 * 2012-10-22
 */
public class IKAnalzyerDemo {
    public static int[] v = new int[64];

    public static void main(String[] args) {
        Analyzer analyzer = new IKAnalyzer();
        TokenStream ts = null;
        try {
            DefaultConfig.getInstance().setUseSmart(true);
            ts = analyzer.tokenStream("myfield", new FileReader("D:\\xx.txt"));
            CharTermAttribute term = ts.addAttribute(CharTermAttribute.class);
            ts.reset();
            while (ts.incrementToken()) {
                phash(hash(term.toString()));
            }
            BigInteger fingerprint = new BigInteger("0");
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < 64; i++) {
                int h = v[i];
                if (h > 0) {
                    sb.append("1");
                    fingerprint = fingerprint.add(new BigInteger("1").shiftLeft(i));
                } else {
                    sb.append("0");
                }
            }
            System.out.println(sb.toString());
            ts.end();
            System.out.println(hammingDistance(new BigInteger("3786178162904268804"), new BigInteger("3795184829599842308")));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (ts != null) {
                try {
                    ts.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static List<String> getWord(String path) throws IOException {
        File file = new File(path);
        InputStream is=null;
        InputStreamReader isr=null;
        BufferedReader reader=null;
        try {
            is = new FileInputStream(file);
            isr = new InputStreamReader(is);
            reader = new BufferedReader(isr);
            String line;
            List<String> list = new ArrayList<>(10240);
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
            return list;
        }catch(IOException e){

        }finally {
            if(is!=null){
                is.close();
            }
            if(isr!=null){
                isr.close();
            }
            if(reader!=null){
                reader.close();
            }
        }
        return null;
    }

    public static void phash(BigInteger word) {
        for (int i = 0; i < 64; i++) {
            BigInteger bitmask = new BigInteger("1").shiftLeft(i);
            if (word.and(bitmask).signum() != 0) {
                v[i] += 1;
            } else {
                v[i] -= 1;
            }
        }
    }

    private static BigInteger hash(String source) {
        if (source == null || source.length() == 0) {
            return new BigInteger("0");
        } else {
            char[] sourceArray = source.toCharArray();
            BigInteger x = BigInteger.valueOf(((long) sourceArray[0]) << 7);
            BigInteger m = new BigInteger("1000003");
            BigInteger mask = new BigInteger("2").pow(64).subtract(new BigInteger("1"));
            for (char item : sourceArray) {
                BigInteger temp = BigInteger.valueOf((long) item);
                x = x.multiply(m).xor(temp).and(mask);
            }
            x = x.xor(new BigInteger(String.valueOf(source.length())));
            if (x.equals(new BigInteger("-1"))) {
                x = new BigInteger("-2");
            }
            return x;
        }
    }
    public static int hammingDistance (BigInteger self, BigInteger otr){
        int tot = 0;
        BigInteger y=self.xor(otr);
        while (y.signum() != 0) {
            tot += 1;
            y = y.and(y.subtract(new BigInteger("1")));
        }
        return tot;
    }
}
