package com.xj.mongodb.builder;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import java.lang.reflect.Field;

/**
 * 构造查询信息及条件
 * @author xiajun
 *
 */
public class QueryBuilder {
	
	/**
	 * 查询条件的构造
	 * @return
	 */
	public DBObject getRef(){
		
		return null;
	}
	/**
	 * 返回字段构造
	 * @return
	 */
	public DBObject getFields(Class clazz,String [] fields){
		DBObject keys = new BasicDBObject();
		if(fields==null){
			Field[] _fields = clazz.getDeclaredFields();
			for (int i = 0; i < _fields.length; i++) {
				String fieldName= _fields[i].getName();
				keys.put(fieldName, 1);
			}
		}else{
			for (int i = 0; i < fields.length; i++) {
				keys.put(fields[i], 1);
			}
		}
		return keys;
	}
}
