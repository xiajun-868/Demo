package com.xj.mongodb.demo;

import com.google.gson.Gson;
import com.mongodb.*;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestDriver<T> {
	private static MongoClient mongo = null;
	private static DB db = null;
	static {
		try {
			mongo = new MongoClient(Arrays.asList(new ServerAddress("10.12.147.196", 27017), new ServerAddress("10.12.147.197",
					27017)));
			db = mongo.getDB("WareExpandPercent");
			db.authenticate("", "".toCharArray());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) throws Exception {
		TestDriver<WareExpandPercentView> test = new TestDriver<WareExpandPercentView>();
		List<String> l = new ArrayList<String>();
		l.add("sku");
		// List<WareExpandPercentView> list =
		// test.findList("WareExpandPercent",null,false,
		// WareExpandPercentView.class);
		//test.distinct("WareExpandPercent");
		//test.group("WareExpandPercent");
		//System.out.println("fsdaf order by".matches("\\s+(o|O)(r|R)(d|D)(e|E)(r|R)\\s+(b|B)(y|Y)"));
		//System.out.println("select * ss order by ss gr".matches(".*?\\s+(o|O)(r|R)(d|D)(e|E)(r|R)\\s+(b|B)(y|Y).*?"));
		//test.inc("WareExpandPercent");
		test.find("WareExpandPercent");
	}
	public void find(String tableName){
		DBCollection collection = db.getCollection(tableName);
		DBObject obj= collection.findOne();
		Gson gson=new Gson();
		System.out.println(obj.toMap());
	}
	public void inc(String tableName){
		DBCollection collection = db.getCollection(tableName);
		BasicDBObject key = new BasicDBObject("thirdType",655);
		//如果是更新多条记录需要使用$set
		//BasicDBObject inc = new BasicDBObject("$set", new BasicDBObject("p7",1));
		BasicDBObject inc = new BasicDBObject("$inc", new BasicDBObject("p7",1));
		collection.update(key, inc , false, true);
	}
	public void distinct(String tableName) {
		DBCollection collection = db.getCollection(tableName);
		BasicDBObject key = new BasicDBObject("thirdType",655);
		List list = collection.distinct("sku",key);
		System.out.println(list.size());
	}
	public void group(String tableName){
		DBCollection collection = db.getCollection(tableName);
		BasicDBObject key = new BasicDBObject("thirdType",true);
		BasicDBObject cond = new BasicDBObject("sku",new BasicDBObject("$gt",1));
		BasicDBObject initial = new BasicDBObject("cou",0);
		String  reduce = "function(obj,pre){pre.cou++}";
		BasicDBList l=(BasicDBList)collection.group(key, cond,initial,reduce);
		System.out.println(l.get(1));
	}
	public List<T> findList(String tableName, List<String> fields,
			Boolean bool, Class clazz) throws Exception {
		DBCollection collection = db.getCollection(tableName);
		List<T> resList = new ArrayList<T>();
		Object obj = null;
		try {
			DBObject ref = new BasicDBObject();
			ref.put("thirdType", 655);
			DBObject keys = new BasicDBObject();
			Field[] _fields = null;
			_fields = clazz.getDeclaredFields();
			if (fields == null || fields.size() <= 0) {
				for (int i = 0; i < _fields.length; i++) {
					String fieldName = _fields[i].getName();
					keys.put(fieldName, 1);
				}
			} else {
				for (int i = 0; i < _fields.length; i++) {
					String fieldName = _fields[i].getName();
					if (!bool && fields.contains(fieldName)) {
						continue;
					} else if (bool && fields.contains(fieldName)) {
						keys.put(fieldName, 1);
					} else if (!bool && !fields.contains(fieldName)) {
						keys.put(fieldName, 1);
					}
				}
			}
			DBObject sort = new BasicDBObject();
			sort.put("sku", -1);
			collection.find(QueryBuilder.start("sku").is(12).get());
			collection.update(ref, keys, true, false);
			List<DBObject> list = collection.find(ref, keys).sort(sort)
					.toArray();
			for (int i = 0; i < list.size(); i++) {
				obj = clazz.getConstructor().newInstance();
				DBObject dbo = list.get(i);
				Method[] methods = clazz.getDeclaredMethods();
				for (int j = 0; j < methods.length; j++) {
					String methodName = methods[j].getName();
					if (methodName.indexOf("get") == 0) {
						continue;
					}
					String m = methodName.substring(3, 4).toLowerCase()
							+ methodName.substring(4, methodName.length());
					if (dbo.get(m) != null) {
						methods[j].invoke(obj, dbo.get(m));
					}
				}
				resList.add((T) obj);
			}
		} catch (Exception e) {
			throw e;
		}
		return resList;
	}
}
