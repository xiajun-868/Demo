package com.xj.protobuf;

import com.google.protobuf.ByteString;
import com.google.protobuf.InvalidProtocolBufferException;

/**
 * User: bjxiajun
 * Date: 14-3-3
 * Time: 下午4:54
 */
public class Demo {
    public static void main(String[] args) throws InvalidProtocolBufferException {
        Test.Person.Builder builder= Test.Person.newBuilder();
        builder.setName("xiajun");
        builder.setId(12);
        builder.setEmail("xiajun_868");
        builder.setSex(Test.Sex.M);
        Test.Person person=builder.build();
        ByteString person_str=person.toByteString();

        Test.Person person2 = Test.Person.parseFrom(person_str);
        System.out.println(person2.getName());
        System.out.println(person2.getEmail());
        System.out.println(person2.getSex().name());
    }
}
