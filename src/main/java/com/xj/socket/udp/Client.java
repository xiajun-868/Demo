package com.xj.socket.udp;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * User: bjxiajun
 * Date: 13-11-1
 * Time: 上午10:32
 */
public class Client {
    public static void start() throws Exception {
        InetAddress serverAddress = InetAddress.getByName("localhost");
        DatagramSocket socket=new DatagramSocket(6189);
        System.out.println("client connection success........");
        byte [] data="client connection".getBytes();
        DatagramPacket packet=new DatagramPacket(data,data.length,serverAddress,6188);
        socket.send(packet);
        while(true){
            DatagramPacket rec_packet=new DatagramPacket(new byte[4*1024],4*1024);
            socket.receive(rec_packet);
            System.out.println(new String(rec_packet.getData(),0,rec_packet.getLength()));
        }
    }

    public static void main(String[] args) throws Exception {
        Client.start();
    }
}
