package com.xj.socket.udp.multicast;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * User: bjxiajun
 * Date: 13-11-1
 * Time: 下午1:47
 */
public class MulticastClient {
    public static void main(String[] args) throws Exception {
        InetAddress maddress = InetAddress.getByName("224.10.10.1");
        MulticastSocket socket = new MulticastSocket();
        socket.joinGroup(maddress);
        byte[] data = "my is cliet msg 2".getBytes();
        DatagramPacket packet = new DatagramPacket(data, data.length, maddress, 5535);
        socket.send(packet);
        byte [] r_data=new byte[1024];
        /*while(true){
            DatagramPacket r_packet=new DatagramPacket(r_data,r_data.length);
            socket.receive(r_packet);
            System.out.println("client: "+new String(r_packet.getData()));
        }*/
    }
}
