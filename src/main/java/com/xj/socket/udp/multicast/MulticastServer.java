package com.xj.socket.udp.multicast;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;

/**
 * User: bjxiajun
 * Date: 13-11-1
 * Time: 下午1:42
 */
public class MulticastServer {
    public static void main(String[] args) throws Exception {
        InetAddress maddress=InetAddress.getByName("224.10.10.1");
        MulticastSocket multicastSocket=new MulticastSocket(5535);
        multicastSocket.joinGroup(maddress);
        byte [] data=new byte[1024];
        while(true){
            DatagramPacket packet=new DatagramPacket(data,data.length);
            multicastSocket.receive(packet);
            System.out.println(new String(packet.getData()));
        }
    }
}
