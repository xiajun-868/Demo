package com.xj.spring.quartz;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

/**
 * author: xiajun
 * Date: 2014-06-23
 * Time: 22-37-00
 */
public class Test2Job implements  Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        System.out.println("=============");
    }
}
