package com.xj.spring.quartz;

import org.quartz.*;

/**
 * author: xiajun
 * Date: 2014-06-23
 * Time: 22-37-00
 */
public class TestJob implements  Job {

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
            String service=(String)context.getJobDetail().getJobDataMap().get("service");
            System.out.println(service+"-----000");
    }
}
