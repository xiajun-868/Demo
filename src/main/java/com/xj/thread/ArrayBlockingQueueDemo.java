package com.xj.thread;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Author: xiajun
 * Date: 2014-06-30
 * Time: 12:14:00
 */
public class ArrayBlockingQueueDemo {
    private static ArrayBlockingQueue queue = new ArrayBlockingQueue(100000);

    public static void put(Object value) {
        try {
            queue.put(value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static Object get() {
        Object r = null;
        try {
            r =  queue.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return r;
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println(System.currentTimeMillis());
        Thread.sleep(15000);
        System.out.println("put start...................");
        for (int i = 0; i < 100000; i++) {
            //ArrayBlockingQueueDemo.put(new Byte[512]);
            Byte []b= new Byte[512];
        }
        Thread.sleep(15000);
        /*System.out.println("get start....................");
        for (int i = 0; i < 2; i++) {
            ConsumeThread ct = new ConsumeThread();
            Thread thread = new Thread(ct);
            thread.setName("consume-" + i);
            thread.start();
        }*/
    }
}

class ConsumeThread implements Runnable {

    @Override
    public void run() {
        while (true) {
            System.out.println(Thread.currentThread().getName() + " -- " + ArrayBlockingQueueDemo.get());
        }
    }
}