package com.xj.thrift.client;

import org.apache.thrift.async.TAsyncClientManager;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.transport.*;

public class ClientDemo {
	public static void main(String[] args) throws Exception {
		Object obj=new Object();
		for (int i = 0; i < 1; i++) {
			AdditionService.Client client = ClientDemo.getNioClient();
			for (int j = 0; j < 1; j++) {
				client.send_add((i+1)*j, 29);
			}
		}
		System.out.println("===================");
		synchronized (obj) {
			obj.wait();
		}
	}

	/**
	 * 阻塞同步客户端
	 * 
	 * @return
	 * @throws org.apache.thrift.transport.TTransportException
	 */
	public static AdditionService.Client getClient() throws TTransportException {
		TTransport transport = new TSocket("127.0.0.1", 9090);
		transport.open();
		// TProtocol protocol = new TBinaryProtocol(transport);
		TProtocol protocol = new TCompactProtocol(transport);
        AdditionService.Client client = new AdditionService.Client(protocol);
		return client;
	}

	/**
	 * 异步客户端
	 *
	 * @return
	 * @throws Exception
	 */
	public static AdditionService.AsyncClient getAsyncClient() throws Exception {
		TNonblockingSocket transport = new TNonblockingSocket("127.0.0.1", 9090);
		TAsyncClientManager acm = new TAsyncClientManager();
		// 使用高密度二进制协议
		TProtocolFactory protocol = new TCompactProtocol.Factory();
		AdditionService.AsyncClient client = new AdditionService.AsyncClient(protocol, acm, transport);
		return client;
	}
	/**
	 * nio服务协议 的阻塞客户端
	 * @return
	 * @throws org.apache.thrift.transport.TTransportException
	 */
	public static AdditionService.Client getNioClient() throws TTransportException {
		TTransport transport = new TFramedTransport(new TSocket("localhost", 9090));
		transport.open();
		TProtocol protocol = new TCompactProtocol(transport);
        AdditionService.Client client = new AdditionService.Client(protocol);
		return client;
	}

	// String s=ClientDemo.getClient().getStr("strs");
	// System.out.println("getstr "+s);
	// System.out.println("qp".hashCode());
	// System.out.println("{}".hashCode());
	/*
	 * String s[]={"rQ","qp","s2"}; String q=""; String q1,q2,q3,q4,q5,q6; for
	 * (int i = 0; i < 3; i++) { q1=s[i%3]; for (int j = 0; j < 3; j++) {
	 * q2=s[j%3]; q2=q1+q2; for (int j2 = 0; j2 < 3; j2++) { q3=s[j2%3];
	 * q3=q2+q3; for (int k = 0; k <3; k++) { q4=s[k%3]; q4=q3+q4; for (int k2 =
	 * 0; k2 < 3; k2++) { q5=q4+s[k2%3]; for (int l = 0; l < 3; l++) {
	 * q6=q5+s[l%3]+"=12&"; q=q+q6; q6=""; } q5=""; } q4=""; } q3=""; } q2=""; }
	 * q1=""; } System.out.println(q);
	 */
}
