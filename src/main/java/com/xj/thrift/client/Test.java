package com.xj.thrift.client;

import org.apache.thrift.TException;
import org.apache.thrift.async.AsyncMethodCallback;

public class Test {
	public static void main(String[] args) throws Exception {
		AdditionService.AsyncClient ay = ClientDemo.getAsyncClient();
		//for (int i = 0; i < 5000; i++) {
			ay.getStr("str23", new AsyncMethodCallback<AdditionService.AsyncClient.getStr_call>() {
				public void onError(Exception exception) {
					exception.printStackTrace();
				}

				public void onComplete(AdditionService.AsyncClient.getStr_call response) {
					try {
						String s = response.getResult();
						System.out.println("异步 " + s);
					} catch (TException e) {
						e.printStackTrace();
					}
				}
			});
		//}
		Thread.sleep(10000);
	}
}
