package com.xj.thrift.server;

import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TProtocolFactory;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.server.TThreadedSelectorServer.Args;
import org.apache.thrift.transport.*;

public class AdditionServiceImpl implements AdditionService.Iface {

	public int add(int n1, int n2) throws TException {
        System.out.println(n1+"   "+System.currentTimeMillis());
		return n1 + n2;
	}

	public static void main(String[] args) {
		AdditionServiceImpl asi = new AdditionServiceImpl();
		AdditionService.Processor<AdditionServiceImpl> processor = new AdditionService.Processor<AdditionServiceImpl>(asi);
		try {
			// TServerTransport serverTransport =new TServerSocket(9090);
			TNonblockingServerTransport socket = new TNonblockingServerSocket(9090);
			TTransportFactory transportFactory = new TFramedTransport.Factory();
			// 使用高密度二进制协议
			TProtocolFactory proFactory = new TCompactProtocol.Factory();
			Args arg = new Args(socket).processor(processor).selectorThreads(4).workerThreads(100)
					.transportFactory(transportFactory).protocolFactory(proFactory);
			arg.maxReadBufferBytes=20480;
			arg.acceptQueueSizePerThread(8);
			TServer server = new TThreadedSelectorServer(arg);
			System.out.println("server 开始.....");
			server.serve();
		} catch (TTransportException e) {
			e.printStackTrace();
		}
	}

	@Override
	public String getStr(String name) throws TException {
		System.out.println("getstr---");
		return name;
	}
}
