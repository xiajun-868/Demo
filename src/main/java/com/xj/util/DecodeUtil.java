package com.xj.util; /**
 * User: bjxiajun
 * Date: 13-12-16
 * Time: 下午2:13
 * 自动识别编码方式
 */

import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CoderResult;

public final class DecodeUtil {

    private static final CharsetDecoder[] DECODER_ARRAY = new CharsetDecoder[]{
            Charset.forName("UTF-8").newDecoder(), Charset.forName("GBK").newDecoder()};

    /**
     * 目前支持UTF-8,GBK,GB2312
     *
     * @param b
     * @return
     */
    public static String decode(byte[] b) {
        ByteBuffer bb = ByteBuffer.wrap(b);
        CharBuffer cb = CharBuffer.allocate(b.length);
        for (CharsetDecoder decoder : DECODER_ARRAY) {
            decoder.reset();
            CoderResult cr = decoder.decode(bb, cb, true);
            if (!cr.isError()) {
                System.out.println(decoder.charset().name());
                decoder.flush(cb);
                break;
            }
        }
        return new String(cb.array(), 0, cb.position());// cb.position() 有效的长度
    }

    /**
     * 指定编码解码
     *
     * @param b
     * @param encode
     * @return
     */
    public static String decode(byte[] b, String encode) {
        ByteBuffer bb = ByteBuffer.wrap(b);
        CharBuffer cb = CharBuffer.allocate(bb.capacity());
        CharsetDecoder decoder = Charset.forName(encode).newDecoder();
        CoderResult cr = decoder.decode(bb, cb, true);
        if (cr.isError()) {// 解码失败
            return new String(b);
        }
        decoder.flush(cb);

        return new String(cb.array(), 0, cb.position());
    }
}