package com.xj.util;

/**
 * Author: xiajun
 * Date: 2014-09-16
 * Time: 10:08:00
 */
public class ExceptionUtil {
    public static String getSimpleInfo(Exception e) {
        String info = null;
        if (e != null) {
            StackTraceElement se = e.getStackTrace()[0];
            StringBuffer sb = new StringBuffer();
            sb.append(se.getClassName());
            sb.append(".");
            sb.append(se.getMethodName());
            sb.append("(),line: ");
            sb.append(se.getLineNumber());
            sb.append(",Exception: ");
            sb.append(e.getClass().getSimpleName());
            sb.append(",massage: ");
            sb.append(e.getMessage());
            info = sb.toString();
        }
        return info;
    }
}
