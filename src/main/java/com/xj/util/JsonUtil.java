package com.xj.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * User: bjxiajun
 * Date: 14-3-14
 * Time: 下午1:17
 */
public class JsonUtil {
    public static void main(String[] args) {
        List<User> list=new ArrayList<User>();
        list.add(new User("zs",12,"hb"));
        list.add(new User("ls",32,"zb"));
        Gson gson=new Gson();
        String str=gson.toJson(list);
        System.out.println(str);
        List<User> li=gson.fromJson("[{\"name\":\"zs\",\"age\":12,\"address\":\"hb\"},{\"name\":\"ls\",\"age\":32," +
                "\"address\":\"zb\"}]",new TypeToken<List<User>>(){}.getType());
        System.out.println(li.get(0).getName());

    }
}
class User{
    public User(String name,int age,String address){
        this.name=name;
        this.address=address;
        this.age=age;
    }
    private String name;
    private int age;
    private String address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}