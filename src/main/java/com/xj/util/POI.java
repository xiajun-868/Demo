package com.xj.util;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Name;
import org.apache.poi.ss.util.CellRangeAddressList;

import java.io.FileOutputStream;

/**
 * poi生成excel的下拉框中添加大量内容时出现错
 * User: xiajun
 * Date: 13-11-5
 * Time: 下午2:56
 */
public class POI {
    public static void main(String[] args) {
        String str = "OA系统管理员, 总裁,技术总监,工程中心主任, 医学营销副总裁, 商务开发与市场高级副总裁, 助理总裁, 执行副总裁、工程中心副主任, 首席工程师、仪器总工艺师, 销售总监, 部门主任（经理）, 部门副主任（经理）, 高级部门主任, 助理主任, 技术组长, 副总裁、工程中心常务副主任, 直销部销售代表, 质控专员, 采购员, 法律事务专员, 项目主管, 业务主管, 秘书, 出纳, 会计, 网络系统管理员, 系统维护工程师, 员工, 司机, CMS通用审核员, 人力资源总监, 人事考勤专员, 人事管理专员, 人事考评专员, 人事培训专员, 车辆管理员, 会议室管理员, 学生, 临时人员, 生产总监, 资产管理员, 物流专员, 部门主任（经理）助理, 部门执行主任（经理）, 质检员, 产品经理, 销售经理, 楼宇资产管理员, 主管总裁, 协管总裁, 运行保障制度文件管理员, 宿舍管理员, 流程档案管理员, CMS安全信息内容发布和审核员, CMS通知公告内容发布员, CMS内部新闻发布和审核员, 公司档案管理员, 办公用品领用代表, 中心副主任, 副总裁, 市场总监, 法律事务经理, 人事副考评专员, 固定资产管理员, 费用负责人, 费用主管总裁, 公司发文部门接收人, 执行组长, 质量检验员, 质量审核员, 物流部请检员, 试剂与耗材部请验员, 高级副总裁, 助理销售总监, 商务审核员, 合同预审员, 合同章保管人, 本部商务, 微阵列服务合同查询角色, 销售管理-财务使用, 物流财务专用角色, 固定资产入库审核员, 部门资产管理员, 试剂销售代表, 渠道销售代表, 客服售后技术人员, 客户服务中心使用功能, HLA合同预审员, 物流部发货员, 生物检测与注册事务副总裁, 市场部礼品管理员, 合同终审员, 生物检测全线产品经理, 销售管理商务开发部, 直销销售总监报表查询, 服务核价数据查询角色人员, 销售管理实习人员, 免疫分析科学总监, 生产信息反馈使用角色, 助理总监, 市场营销财务票据查询角色, IT部经理, 仪器生产主管总裁审核组, 事业部总经理, 物料质检接收人, 市场部礼品资料管理员, IT部资产查询管理员, 医学技术事业部发货查询员, CMS人事信息审核员, 物流部存货管理员, 医学技术事业部预审员, CMS支部工作采风内容发布审核员, CMS工会聚焦内容发布员, CMS工会聚焦审核员, 医学技术华东区内勤, 微阵列考评员, 微阵列考评查询（全部）, 试剂耗材小组报表查询角色, IT服务工程师, 非销售合同查询员, 市场副总裁, 公司管理者代表, 退换发票查询角色（上海）, 移液器自校准审核员, 仪器生产部请验员, 仪器生产部报表查询角色(全部), 非定制产品成本核算与定价角色, 医学技术A区内勤, 医学技术C区内勤, 业务员工作日报表查询角色（全部）, 备份负责人, 微阵列到款查询角色, 投融资总监, 物流总监, CMS质量动态内容发布员, CMS质量动态审核员, 高层代办秘书, 专利办公室主任, 微阵列服务客户来访查询角色, IT系统帐号信息报表查询角色, I新建IT信息帐号角色, 人力资源调查问卷发起和查询角色, 仪器成品半成品请验单查询角色(仪器组), 国际订单审核物流部查询角色, 固定资产可查询角色, 接待服务办理人, 客服接收人, 试剂组负责人, 仪器组负责人, 维修管理员, 固定资产归属部门待确认查询角色, CMS栏目管理角色, 会议室调配员, 签署销售合同或协议查询角色, 销售副总监, 物流部报表查询角色, 国际商务开发部发货员, 技术支持总监, 公共关系总监, 技术副总监, 差旅费报销人力资源办理人, 非定制核价营销管理部审核人, 非定制核价财务审核人, 办公用品库房管理员, 办公用品管理员, 国内物流部发货员, 渠道销售服务合同终审员, HLA合同终审员, 即时库存查询角色, 产品要求评审研发部门办理人, 产品要求评审生产部门办理人, 产品要求评审质控小组办理人, 项目立项审批角色, 大客户部内勤, 生物科技事业部内勤, 遗传检测部内勤, 物流部试剂质检角色, 专利录入人员, 技术支持中心主任, 生物科技事业部仪器试用流程使用人, 财务总监, 知识产权调度人, 微阵列实验平台组长, 大区经理, 试剂生产车间物料送检员, 医学秘书, 部门负责人审批角色, 主管总裁审批角色, 物料促销流程使用人, 爱身谱检测流程使用人, 质量管理部流程管理员, CMS企业文化和员工活动内容发布员, 博康内勤, HLA忘忧谱审核员, 数据审核员, 报告订送员, 博康流程起草人, CMS企业文化和员工活动内容审核员, 仪器过程小组, 质量管理部仪器小组, 业务员日报表查看（临时）, 采购经理, 物流采购报表查询, 食物不耐受测服务流程使用人, 博软销售流程起草人, 人事福利专员, CMS财务通知发布员, 即时库存管理员, 应用开发工程师, 实习生, 软件工程师, 研究助理, 行政助理, 实验员, 研究科学家, 技术员, 信息主管, 技术支持, 工艺技术员, 工艺师, android开发工程师, 采购主管, 市场专员, C++开发工程师, 生产文员, 高级研究助理, 试验员, 商务助理, 销售代表, 机械工艺工程师, 人力资源部主管, 机械工程师, JAVA开发工程师, 电子工程师, 华南大区经理, 质量管理员, 销售助理, 成都项目流程审批启动专用, 仪器测试工艺师, 质量工艺工程师, 高级仪器机械装配师, 工艺员, 总经理助理, 生产技术员, 仪器电气工艺工程师, 研发助理, 广州区域技术支持, 政府事务助理经理, 文件管理员, 济南区域技术支持, 销售代表(武汉), 销售经理（黑龙江）, 政府事务经理, 部门主任助理, 光学工程总监, 实施项目经理, 仓库管理员, 组织结构调整角色, 品牌宣传专员, 项目管理专员, 数据分析员, 生物信息助理, 进出口主管, 测试工程师, 区域经理, 遗传咨询师, 医学咨询师, CMS人力相关版块发布员, 主管会计, 软件事业部总经理, 项目助理, 前台, 行政主管, 质量体系专员, JAVA开发技术经理, 产品专员, 计量技术员, 高级研究科学家, WEB前端开发工程师";
        String s[] = str.split(",");
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet sheet = wb.createSheet("下拉框");
        HSSFSheet hidden = wb.createSheet("hidden");
        for (int i = 0, length= s.length; i < length; i++) {
            String name = s[i];
            HSSFRow row = hidden.createRow(i);
            HSSFCell cell = row.createCell(0);
            cell.setCellValue(name);
        }
        Name namedCell = wb.createName();
        namedCell.setNameName("hidden");
        namedCell.setRefersToFormula("hidden!A1:A" + s.length);

        HSSFRow row = sheet.createRow(0);
        HSSFCell cell = row.createCell(0);
        cell.setCellValue("请选择");
        CellRangeAddressList regions = new CellRangeAddressList(0, 0, 0, 0);
        DVConstraint constraint = DVConstraint.createFormulaListConstraint("hidden");
        HSSFDataValidation validation = new HSSFDataValidation(regions, constraint);
        sheet.addValidationData(validation);
        wb.setSheetHidden(1, true);
        FileOutputStream fileOut;
        try {
            fileOut = new FileOutputStream("D:/test.xls");
            wb.write(fileOut);
            fileOut.close();
            System.out.print("over......");
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}
