package com.xj.util.tree.bplus2;

/**
 * B+ tree
 * User: bjxiajun
 * Date: 13-11-6
 * Time: 下午1:05
 */
public interface B {
    /**
     * 获取值
     * @param key
     * @return
     */
    public Object get(Comparable key);

    /**
     * 删除key
     * @param key
     * @return
     */
    public void remove(Comparable key);
    public void insertOrUpdate(Comparable key, Object value);
}
