package com.xj.zookeeper.client;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.ZooDefs.Ids;

import java.util.List;

/**
 * @author ruodao
 * @since 1.0
 * 2010-2-18 下午09:03:21
 */
public class ZooKeeperOperator extends AbstractZooKeeper {
	/**
	 * 创建持久态的znode,不支持多层创建.比如在创建/parent/child的情况下,无/parent.无法通过.
	 * @param path eg:  /parent/child1
	 * @param data
	 * @throws InterruptedException 
	 * @throws org.apache.zookeeper.KeeperException
	 */
	public void create(String path,byte[] data) throws KeeperException, InterruptedException{
		//this.zooKeeper.create(path, data, Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT/*此处创建的为持久态的节点,可为瞬态*/);
		this.zooKeeper.create(path, data, Ids.CREATOR_ALL_ACL, CreateMode.EPHEMERAL);
		System.out.println("----------创建节点成功----------");
    }

	public void delete(String path) throws InterruptedException, KeeperException {
		this.zooKeeper.delete(path, -1);
	}
	/**
	 * 获取节点的孩子信息
	 * @param path
	 * @throws org.apache.zookeeper.KeeperException
	 * @throws InterruptedException
	 */
	public void getChild(String path) throws KeeperException, InterruptedException{
		try {
			List<String> children = this.zooKeeper.getChildren(path, this,null);
			if (children.isEmpty()) {
				System.out.printf("没有节点在%s中.", path);
				return;
			}else{
				System.out.printf("节点%s中存在的节点:%n", path,"\n");
				for(String child: children){
					System.out.println(child);
				}
			}
		} catch (KeeperException.NoNodeException e) {
			System.out.printf("%s节点不存在.", path);
			throw e;
		}
	}

	public byte[] getData(String path) throws KeeperException, InterruptedException {
		return	this.zooKeeper.getData(path, true,null);
	}
	public void setData(String path,byte[] data) throws KeeperException, InterruptedException{
		this.zooKeeper.setData(path, "xiajun007".getBytes(), -1);
	}
	public static void main(String[] args) {
		try {
			System.out.println("-----------开始连接-------------");
			ZooKeeperOperator zkoperator= new ZooKeeperOperator();
			zkoperator.connect("10.12.147.196:2181");
			System.out.println("------------连接成功-------------");
			byte[] data = new byte[]{'d','a','t','a'};
			//WriteLock 
			//zkoperator.delete("/root");
			//zkoperator.delete("/root/child1");
			zkoperator.create("/root",null);
			//System.out.println(Arrays.toString(zkoperator.getData("/root/child1")));
			
			//zkoperator.create("/root/child1",data);
			//zkoperator.setData("/root/child1", null);
			//System.out.println(Arrays.toString(zkoperator.getData("/root/child1")));
			
			//zkoperator.create("/root/child2",data);
			//System.out.println(Arrays.toString(zkoperator.getData("/root/child2")));
			
			//System.out.println("节点孩子信息:");
			//System.out.println(zkoperator.getData("/root/child1"));
			//WatchedEvent event= new WatchedEvent(EventType.NodeDeleted, KeeperState.SyncConnected, "/root");
			synchronized (zkoperator) {
				zkoperator.wait();
			}
			zkoperator.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
