package com.xj.zookeeper.framework;

import org.apache.zookeeper.Watcher.Event;

import java.net.SocketException;

/**
 * Author: xiajun
 * Date: 2014-09-16
 * Time: 19:40:00
 * 节点监听
 */
public abstract class Listener {
    protected String node;

    /**
     *
     * @param node 监控的节点
     */
    public Listener(String node) {
        this.node = node;
    }

    /**
     * 对指定路径的值节点进行监听
     * @param change 发生改变的节点(可能是子节点)
     * @param eventType 事件类型(创建，删除)
     * @param data      监控数据变化时返回的数据，监控节点时为null
     */
    public abstract void listen(String change,Event.EventType eventType, byte [] data) throws ZkException, SocketException;

    public String getNode() {
        return node;
    }
}
