package com.xj.zookeeper.framework;

import org.apache.zookeeper.Watcher.Event;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: xiajun
 * Date: 2014-09-19
 * Time: 17:14:00
 */
public class ListenerManager {
    private Listener listener;
    private List<String> clientNode = new ArrayList<String>();
    private String client;
    private byte[] data;
    private Event.EventType eventType;
    public ListenerManager() {}

    public ListenerManager(Listener listener) {
        this.listener = listener;
    }


    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void setClientNode(List<String> clientNode) {
        this.clientNode = clientNode;
    }

    public List<String> getClientNode() {
        return clientNode;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public Event.EventType getEventType() {
        return eventType;
    }

    public void setEventType(Event.EventType eventType) {
        this.eventType = eventType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
