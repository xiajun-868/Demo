package com.xj.zookeeper.framework;

/**
 * Author: xiajun
 * Date: 2014-09-16
 * Time: 16:35:00
 */
public class Node {
    private String path;
    private byte [] data;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public byte[] getData() {
        return data.clone();
    }

    public void setData(byte[] data) {
        this.data = data.clone();
    }
}
