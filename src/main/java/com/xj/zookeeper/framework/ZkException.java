package com.xj.zookeeper.framework;

/**
 * Author: xiajun
 * Date: 2014-09-16
 * Time: 14:11:00
 */
public class ZkException extends Exception {
    public ZkException(String msg) {
        super(msg);
    }

    public ZkException(String message,Exception e) {
        super(message,e);
    }
}
