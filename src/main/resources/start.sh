#!/bin/bash
path=$(cd "$(dirname "$0")"; pwd)
libPath="$path/../lib"
confPath="$path/../conf"
logsPath="$path/../logs"
CLASSPATH=""
PID="$path/../PID"
MAINCLASS="com.xj.Test"
pid=`ps -ef|grep -v grep |grep $MAINCLASS|awk 'NR==1{print $2}'`
if [  $pid -gt 0 ];then
	echo "************************************************"
	echo "*                                              *"
	echo "*                                              *"
	echo "*  ERROR: The service has been start,pid=$pid. *"
	echo "*                                              *"
	echo "*                                              *"
	echo "************************************************"
	exit 1
fi


if [ ! -d $logsPath ];then
	mkdir -p $logsPath
fi
if [ -d $libPath ];then
	for jar in $libPath/*.jar
	do
		CLASSPATH=$CLASSPATH:$jar
	done
fi
if [ -d $confPaht ];then
	CLASSPATH=$CLASSPATH:$confPath
fi
echo "CLASSPATH: $CLASSPATH"
nohup java -cp $CLASSPATH $MAINCLASS >$logsPath/sto.log 2>&1 &
echo $! >$PID
echo "PID = `cat $PID`"
echo "LOGS = $logsPath/sto.log"
