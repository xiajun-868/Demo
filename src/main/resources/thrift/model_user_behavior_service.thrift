
/*
 * 用户行为分析接口
 */

namespace java com.jd.si.model.user.behavior
namespace cpp si_model_user_behavior
namespace perl si_model_user_behavior
namespace php si_model_user_behavior

enum ResultCode {               //响应码
    OK = 0,                     //正常返回
    UNKNOWN_ERROR = 1           //未知错误
    USER_NOT_EXIST = 101        //用户不存在
}

struct TimeSpan {          //导出数据的时间段
    1: optional i64 start_time //开始时间戳
    2: optional i64 end_time  //结束时间戳
}

struct SKUUserBehaviorStats
{
    1: optional i64 skuid
    2: optional i64 num_clicks
    3: optional i64 num_orders
    4: optional i64 num_follows
}

// dongw: get a list of skus for one or more categories, sorted by time
// get a list of 3rd level categries sorted by time

struct GetUserBehaviorRequest {      //请求参数
    1: optional bool debug;
    2: optional string user_pin;                 // 用户pin
    3: optional TimeSpan click_time_span;
    4: optional bool need_sku_click;
    5: optional TimeSpan follow_time_span;
    6: optional bool need_sku_follow;
    7: optional TimeSpan order_time_span;
    8: optional bool need_sku_order;
    9: optional TimeSpan search_time_span;
    10: optional bool need_search_key;
}

struct GetUserBehaviorResult {              //响应数据

    1: optional ResultCode result_code = ResultCode.OK //相应码默认为OK
    2: optional string explain              //错误码描述
    3: optional string user_pin;        // 用户pin,用于客户端校验，防止响应与请求串行
    4: optional list<SKUUserBehaviorStats> skus
    5: optional list<string> search_keys    //用户的搜索词
} 

service UserBehaviorService {
    GetUserBehaviorResult getUserBehavior(1: GetUserBehaviorRequest request)
}
