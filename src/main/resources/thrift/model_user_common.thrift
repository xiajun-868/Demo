//******************** 用户主观模型相关服务共用的定义文件 *******************

namespace java com.jd.si.model.user.common
namespace cpp si_model_user_common
namespace py si_model_user_common
namespace php si_model_user_common
namespace perl si_model_user_common

enum ResultCode
{
    OK = 0,
    UNKNOWN_ERROR = 1,
    USER_NOT_EXIST = 101,
    MODEL_NOT_EXIST = 102
}

enum CategoryLevel
{
	LEVEL_1 = 1,
	LEVEL_2 = 2,
	LEVEL_3 = 3
}

struct Category
{ 
    1:i32 category_id  // i64
    2:CategoryLevel category_level
}

//应用授权
struct AppToken
{
		1: optional i32 app_id
		2: optional string app_key
}
