namespace java com.test

struct RS {
   1:i32 code=0;
   2:string id;
   3:list<RRL>  children;
   4:string account
}

struct RRL {
   1:string id;
   2:string name;
   2:string desc
}

service CWS {
   RS getUserInfo(1:string name)
}